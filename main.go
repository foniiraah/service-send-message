package main

import (
	// "fmt"
	"fmt"
	"log"

	// "net/http"
	"net/smtp"
	"strings"

	// "gopkg.in/gomail.v2"
	"ServiceSendMessage/config"
	"ServiceSendMessage/controller"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

const CONFIG_SMTP_HOST = "smtp.gmail.com"

const CONFIG_SMTP_PORT = 587
const CONFIG_SENDER_NAME = "Team Send Message Anonym <teamsendmessageanonym@gmail.com>"
const CONFIG_AUTH_EMAIL = "teamsendmessageanonym@gmail.com"
const CONFIG_AUTH_PASSWORD = "nryk hgbz zcix zkrn"

func main() {

	connMySql, err := config.ConMySql()
	if err != nil {
		log.Println("cause err", err)
		return
	}
	log.Println("connection mysql", connMySql)

	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.CORS())
	//  e.Use(middleware.Recover())

	// // handle cors
	// http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	// 	w.Header().Set("Access-Control-Allow-Origin", "*")
	// 	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT")
	// 	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, X-CSRF-Token")

	// 	if r.Method == "OPTIONS" {
	// 		w.Write([]byte("allowed"))
	// 		return
	// 	}

	// 	w.Write([]byte("hello"))
	// })

	// Routes
	routeMessage := e.Group("/api")
	routeMessage.POST("/create", controller.CreateMessage)
	routeMessage.GET("/loadData", controller.GetAllData)

	// Start server
	// e.Logger.Fatal(e.Start(":1323"))
	// http.ListenAndServe(":1323", nil)

	to := []string{"foniiraah@gmail.com"}
	cc := []string{}
	subject := "foniiraah@gmail.com"
	message := "Hello"

	err = sendMail(to, cc, subject, message)
	if err != nil {
		log.Fatal(err.Error())
	}

	log.Println("Mail sent!")
}

func sendMail(to []string, cc []string, subject, message string) error {
	body := "From: " + CONFIG_SENDER_NAME + "\n" +
		"To: " + strings.Join(to, ",") + "\n" +
		"Cc: " + strings.Join(cc, ",") + "\n" +
		"Subject: " + subject + "\n\n" +
		message

	auth := smtp.PlainAuth("", CONFIG_AUTH_EMAIL, CONFIG_AUTH_PASSWORD, CONFIG_SMTP_HOST)
	smtpAddr := fmt.Sprintf("%s:%d", CONFIG_SMTP_HOST, CONFIG_SMTP_PORT)

	err := smtp.SendMail(smtpAddr, auth, CONFIG_AUTH_EMAIL, append(to, cc...), []byte(body))
	if err != nil {
		return err
	}

	return nil
}
