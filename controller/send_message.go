package controller

import (
	"ServiceSendMessage/config"
	"ServiceSendMessage/model"
	"log"

	"fmt"

	"github.com/labstack/echo/v4"

	// "net/http"
	"net/smtp"
	"strings"
)

type Response struct {
	Status     bool        `json:"status"`
	ErrMessage string      `json:"err_message"`
	Message    string      `json:"message, onitempty"`
	Data       interface{} `json:"data, onitempty"`
}

const CONFIG_SMTP_HOST = "smtp.gmail.com"

const CONFIG_SMTP_PORT = 587
const CONFIG_SENDER_NAME = "Team Send Message Anonym <teamsendmessageanonym@gmail.com>"
const CONFIG_AUTH_EMAIL = "teamsendmessageanonym@gmail.com"
const CONFIG_AUTH_PASSWORD = "nryk hgbz zcix zkrn"

func GetAllData(c echo.Context) error {
	connMySql, err := config.ConMySql()
	if err != nil {
		log.Println("cause err", err)
		return c.JSON(500, Response{
			Status:     false,
			ErrMessage: "Bad Request",
		})
	}

	// matiin koneksi
	defer connMySql.DB.Close()
	// log.Println("connection mysql", connMySql)

	sqlRow, err := connMySql.DB.Query("SELECT * FROM tbl_data")
	if err != nil {
		return c.JSON(500, Response{
			Status:     false,
			ErrMessage: "Field Tidak Sesuai",
		})
	}
	defer sqlRow.Close()

	var results []model.SendMessageResponse
	for sqlRow.Next() {
		var result = model.SendMessageResponse{}
		err = sqlRow.Scan(&result.Id, &result.Nama_pengirim, &result.Email_penerima, &result.Isi_email, &result.Email_pengirim)
		if err != nil {
			return c.JSON(500, Response{
				Status:     false,
				ErrMessage: err.Error(),
			})
		}
		results = append(results, result)
	}

	return c.JSON(201, Response{
		Status:  true,
		Message: "Success",
		Data:    results,
	})
}

func CreateMessage(c echo.Context) error {
	var body = new(model.SendMessageRequest)
	err := c.Bind(&body)
	if err != nil {
		return c.JSON(500, Response{
			Status:     false,
			ErrMessage: "invalid post",
		})
	}

	connMySql, err := config.ConMySql()
	if err != nil {
		log.Println("cause err", err)
		return c.JSON(500, Response{
			Status:     false,
			ErrMessage: "Bad Request",
		})
	}

	// matiin koneksi
	defer connMySql.DB.Close()
	// log.Println("connection mysql", connMySql)

	sqlRes, err := connMySql.DB.Exec("INSERT INTO tbl_data VALUES(?, ?, ?, ?,?)", nil, body.Nama_pengirim, body.Email_pengirim, body.Isi_email, body.Email_penerima, body.CC, body.Subject)
	if err != nil {
		return c.JSON(500, Response{
			Status:     false,
			ErrMessage: "Field Tidak Sesuai",
		})
	}
	id, _ := sqlRes.LastInsertId()

	return c.JSON(201, Response{
		Status:  true,
		Message: "Success",
		Data: map[string]interface{}{
			"id":             id,
			"nama pengirim":  body.Nama_pengirim,
			"email pengirim": body.Email_pengirim,
			"email penerima": body.Email_penerima,
			"isi email":      body.Isi_email,
		},
	})
}

func Sent() {
	to := []string{"foniiraah@gmail.com"}
	cc := []string{}
	subject := "foniiraah@gmail.com"
	message := "Hello"

	var err error
	err = sendMail(to, cc, subject, message)
	if err != nil {
		log.Fatal(err.Error())
	}

	log.Println("Mail sent!")
}

func sendMail(to []string, cc []string, subject, message string) error {
	body := "From: " + CONFIG_SENDER_NAME + "\n" +
		"To: " + strings.Join(to, ",") + "\n" +
		"Cc: " + strings.Join(cc, ",") + "\n" +
		"Subject: " + subject + "\n\n" +
		message

	auth := smtp.PlainAuth("", CONFIG_AUTH_EMAIL, CONFIG_AUTH_PASSWORD, CONFIG_SMTP_HOST)
	smtpAddr := fmt.Sprintf("%s:%d", CONFIG_SMTP_HOST, CONFIG_SMTP_PORT)

	err := smtp.SendMail(smtpAddr, auth, CONFIG_AUTH_EMAIL, append(to, cc...), []byte(body))
	if err != nil {
		return err
	}

	return nil
}
