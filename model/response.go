package model

type SendMessageResponse struct {
	Id             int    `json:"id"`
	Nama_pengirim  string `json:"Nama_pengirim"`
	Email_pengirim string `json:"Email_pengirim"`
	Isi_email      string `json:"Isi_email"`
	Email_penerima string `json:"Email_penerima"`
	CC             string `json:"cc"`
	Subject        string `json:"Subject"`
}
