package config

import (
	"database/sql"
	"log"

	// "time"

	_ "github.com/go-sql-driver/mysql"
)

type MySQLDB struct {
	DB *sql.DB
}

func ConMySql() (*MySQLDB, error) {
	db, err := sql.Open("mysql", "root@tcp(localhost:3306)/send_message")
	if err != nil {
		log.Println("error cause", err)
		return nil, err
	}
	// See "Important settings" section.
	// db.SetConnMaxLifetime(time.Minute * 3)
	// db.SetMaxOpenConns(10)
	// db.SetMaxIdleConns(10)
	log.Println("test connection", db.Ping())

	mySQLDB := MySQLDB{
		DB: db,
	}
	return &mySQLDB, nil
}
